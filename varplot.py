import sys
import os
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from copy import deepcopy

BROJEVI = False
 
tip = int(sys.argv[1])
filename = ('single', 'general')[tip] + '_vary.out'
with open(filename, 'r') as f:
    lines = f.readlines()
i = 0
res = dict()
time = dict()
dev = dict()
while '---\n' in lines:
    j = lines.index('---\n')
    lines[j] = None
    if j - i < (190, 168)[tip]:
        i = j + 1
        continue
    for k in range(i, j):
        linija = lines[k].split()
        if len(linija) == 4:
            m, n, s, tp = map(int, linija)
            alg = 0
            continue
        if len(linija) == 0:
            continue
        r, t, d = float(linija[0]), float(linija[1]), float(linija[2])
        key = (alg, m, n, s, tp)
        if key not in res:
            res[key] = []
            time[key] = []
            dev[key] = []
        res[key].append(r)
        time[key].append(t)
        dev[key].append(d)
        alg += 1
    i = j + 1

font = {'size': 20}
matplotlib.rc('font', **font)

cols = {'SS-VAM': 'g', 'SS-TSM': 'b', 'AP': 'pink', 'MIP': 'r',
        'Clus2-MIP': 'purple', 'Clus3-MIP': 'orange'}

def avg(a):
    return sum(a) / len(a)

def f(d):
    if d == res:
        return 'percentage of satisfied QoS reqs.'
    if d == time:
        return 'execution time [s]'
    if d == dev:
        return 'QoS improvement'

def g(d):
    if d == res:
        return 'accuracy'
    if d == time:
        return 'time'
    if d == dev:
        return 'deviation'

if tip == 0:
    algs = ['AP', 'SS-VAM', 'SS-TSM']
    stil = ['r--v', 'g-o', 'b-o']
    fill = ['full', 'none', 'full']
    for tp in (0, 1):
        title = ('Low', 'High')[tp] + ' THR'
        for d in (res, time):
            plt.xlabel('Number of users')
            plt.ylabel('Avg. {}'.format(f(d)))
            ymin, ymax = 10, 0
            for alg in (2, 1, 0):
                xx = list(range(50, 501, 50))
                vals = [avg(d[(alg, x, 1, 100, tp)]) for x in xx]
                p = plt.plot(xx, vals, stil[alg], fillstyle=fill[alg], label=algs[alg])
                x, y = p[0].get_data()
                for i, j in zip(x, y):
                    if BROJEVI:
                        plt.annotate("{:.2f}".format(j), xy=(i, j), size=12, )
                ymin = min(ymin, 0.9 * min(vals))
                ymax = max(ymax, 1.05 * max(vals))
            plt.legend(loc='best')
            #plt.title(title)
            if d == res:
                plt.axis([20, 510, 0.95, 1.001])
            else:
                plt.axis([20, 510, 0, ymax])
            #plt.show()
            plt.gcf().subplots_adjust(bottom=0.15, left=0.15)
            plt.savefig('SingleTask_' + g(d) + '_vs_users_' + ('low', 'high')[tp] +\
                    '_THR.pdf', format='pdf')
            plt.close()

            plt.xlabel('Number of services')
            plt.ylabel('Avg. {}'.format(f(d)))
            ymin, ymax = 10, 0
            for alg in (2, 1, 0):
                xx = list(range(40, 201, 20))
                vals = [avg(d[(alg, 300, 1, x, tp)]) for x in xx]
                ymin = min(ymin, 0.9 * min(vals))
                ymax = max(ymax, 1.05 * max(vals))
                p = plt.plot(xx, vals, stil[alg], fillstyle=fill[alg], label=algs[alg])
                x, y = p[0].get_data()
                for i, j in zip(x, y):
                    if BROJEVI:
                        plt.annotate("{:.2f}".format(j), xy=(i, j), size=12, )
            if d == res:
                plt.axis([30, 210, 0.95, 1.001])
            else:
                plt.axis([30, 210, 0, ymax])
            plt.legend(loc='best')
            #plt.title(title)
            #plt.show()
            plt.gcf().subplots_adjust(bottom=0.15, left=0.15)
            plt.savefig('SingleTask_' + g(d) + '_vs_services_' + ('low', 'high')[tp] +\
                    '_THR.pdf', format='pdf')
            plt.close()

if tip == 1:
    algs = ['MIP', 'SS-VAM', 'SS-TSM', 'GreedyMIP']
    stil = ['r--^', 'g-o', 'b-o', 'm--s']
    fill = ['full', 'none', 'full', 'full']
    for d in (res, time, dev):
        plt.xlabel('Number of users')
        plt.ylabel('Avg. {}'.format(f(d)))
        ymin, ymax = 100, 0
        for alg in (3, 2, 1, 0):
            xx = list(range(20, 201, 20))
            vals = [avg(d[(alg, x, 8, 200, -1)]) for x in xx]
            p = plt.plot(xx, vals, stil[alg], fillstyle=fill[alg], label=algs[alg])
            x, y = p[0].get_data()
            for i, j in zip(x, y):
                if BROJEVI:
                    plt.annotate("{:.2f}".format(j), xy=(i, j), size=12, )
            ymin = min(ymin, 0.9 * min(vals))
            ymax = max(ymax, 1.05 * max(vals))
        plt.legend(loc='best')
        if d == res:
            plt.axis([10, 207, 0.95, 1.001])
        else:
            plt.axis([10, 207, 0, ymax])
        #plt.show()
        plt.gcf().subplots_adjust(bottom=0.15, left=0.15)
        plt.savefig('General_' + g(d) + '_vs_users.pdf', format='pdf')
        plt.close()

        plt.xlabel('Number of services')
        plt.ylabel('Avg. {}'.format(f(d)))
        ymin, ymax = 100, 0
        for alg in (3, 2, 1, 0):
            xx = list(range(80, 401, 40))
            vals = [avg(d[(alg, 100, 4, x, -1)]) for x in xx]
            ymin = min(ymin, 0.9 * min(vals))
            ymax = max(ymax, 1.05 * max(vals))
            p = plt.plot(xx, vals, stil[alg], fillstyle=fill[alg], label=algs[alg])
            x, y = p[0].get_data()
            for i, j in zip(x, y):
                if BROJEVI:
                    plt.annotate("{:.2f}".format(j), xy=(i, j), size=12, )
        if d == res:
            plt.axis([60, 410, 0.95, 1.001])
        else:
            plt.axis([60, 410, 0, ymax])
        plt.legend(loc='best')
        #plt.show()
        plt.gcf().subplots_adjust(bottom=0.15, left=0.15)
        plt.savefig('General_' + g(d) + '_vs_services.pdf', format='pdf')
        plt.close()

        plt.xlabel('Number of tasks')
        plt.ylabel('Avg. {}'.format(f(d)))
        ymin, ymax = 100, 0
        for alg in (3, 2, 1, 0):
            xx = list(range(2, 11))
            vals = [avg(d[(alg, 100, x, 200, -1)]) for x in xx]
            ymin = min(ymin, 0.9 * min(vals))
            ymax = max(ymax, 1.05 * max(vals))
            p = plt.plot(xx, vals, stil[alg], fillstyle=fill[alg], label=algs[alg])
            x, y = p[0].get_data()
            for i, j in zip(x, y):
                if BROJEVI:
                    plt.annotate("{:.2f}".format(j), xy=(i, j), size=12, )
        if d == res:
            plt.axis([0.5, 10.5, 0.95, 1.001])
        else:
            plt.axis([0.5, 10.5, 0, ymax])
        plt.legend(loc='best')
        #plt.show()
        plt.gcf().subplots_adjust(bottom=0.15, left=0.15)
        plt.savefig('General_' + g(d) + '_vs_tasks.pdf', format='pdf')
        plt.close()
