from scipy.optimize import linear_sum_assignment
from util import *
import numpy as np

@timeit
def solve(test):
    m, n, qos_dims, user_reqs, _, qos, tp = test
    qos = qos[0]
    tp = tp[0]
    n = len(tp)
    utility = np.zeros((m, n))
    for u in range(m):
        for i in range(n):
            utility[u, i] = sum(user_reqs[u, d] >= qos[d][u, i] for d in range(qos_dims))

    load = [0 for x in tp]
    for u in range(m):
        load[np.argmax(utility[u])] += 1
    if all(load[i] <= x for i, x in enumerate(tp)):
        ret = sum(np.max(utility[u]) for u in range(m)) / (m * qos_dims)
        #print('AP greedy worked', ret)
        return ret

    service_index = []
    wcost = []
    for i, c in enumerate(tp):
        for j in range(c):
            service_index.append(i)
            wcost.append(qos_dims - utility[:, i])
    row_ind, col_ind = linear_sum_assignment(wcost)
    ret = 0
    for i, row in enumerate(row_ind):
        user = col_ind[i]
        service = service_index[row]
        ret += utility[user, service]
    ret /= (m * qos_dims)
    #print('AP', ret)
    return ret
