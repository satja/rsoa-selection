from copy import deepcopy
from pulp import *
from numpy.linalg import norm
from sklearn.cluster import KMeans
import numpy as np
import sys
from util import *
from time import time

@timeit
def solve(test, num_clusters):
    m, n, qos_dims, user_reqs, user_class_demand, qos, tp = test

    qos = [[qos[c][d][0] for d in range(qos_dims)] for c in range(n)]

    # Phase 1: Tenant Clustering
    for d in range(qos_dims):
        mini, maxi = min(user_reqs[:, d]), max(user_reqs[:, d])
        user_reqs[:, d] = (maxi - user_reqs[:, d]) / (maxi - mini)
    kmeans = KMeans(num_clusters).fit(user_reqs)

    # Phase 2: Similarity Mapping
    centroids = kmeans.cluster_centers_

    # Phase 3: Service Clustering
    clusters = [[] for c in range(n)]
    for c in range(n):
        for d in range(qos_dims):
            mini, maxi = min(qos[c][d]), max(qos[c][d])
            qos[c][d] = (qos[c][d] - mini) / (maxi - mini)
        for i, _ in enumerate(tp[c]):
            vec = np.array([qos[c][d][i] for d in range(qos_dims)])
            distances = np.array([norm(vec - x) for x in centroids])
            clusters[c].append(np.argmin(distances))

    # Phase 4: Service Ranking
    ranking = [[] for c in range(n)]
    for c in range(n):
        ranking[c] = [[] for k in range(num_clusters)]
        for k in range(num_clusters):
            ranking[c][k] = [i for i, x in enumerate(clusters[c]) if x == k]
            for i in range(len(ranking[c][k])):
                for j in range(i + 1, len(ranking[c][k])):
                    a = ranking[c][k][i]
                    b = ranking[c][k][j]
                    if sum(qos[c][d][a] for d in range(qos_dims)) <\
                            sum(qos[c][d][b] for d in range(qos_dims)):
                        ranking[c][k][i], ranking[c][k][j] = b, a

    m, n, qos_dims, user_reqs, user_class_demand, qos, tp = test

    # Phase 5: Service Recommendation for Selection
    r = 0
    while True:
        prob = LpProblem('clus_mip', LpMinimize)
        services = [[] for c in range(n)]
        exhausted = True
        num_s = [0 for c in range(n)]
        for c in range(n):
            for k in range(num_clusters):
                if 2 ** r < len(ranking[c][k]):
                    exhausted = False
                    services[c] += ranking[c][k][:2 ** r]
                else:
                    services[c] += ranking[c][k]
            num_s[c] = len(services[c])
        x = LpVariable.dicts('choice',
                (list(range(m)), list(range(n)), list(range(max(num_s)))),
                0, 1, LpInteger)
        for u in range(m):
            for c in range(n):
                prob += lpSum(x[u][c][i] for i in range(num_s[c])) == user_class_demand[u][c], ""
        for c in range(n):
            for i, s in enumerate(services[c]):
                prob += lpSum(x[u][c][i] for u in range(m)) <= tp[c][s], ""
        for u in range(m):
            for d in range(qos_dims):
                prob += (float(user_reqs[u][d]) >= lpSum(
                    lpSum(qos[c][d][u, s] * x[u][c][i] for i, s in enumerate(services[c]))
                    for c in range(n)
                    ))
        prob.solve()

        # Check the solution.
        satisfied = 0
        wrong = False
        deviations = [[] for d in range(qos_dims)]
        for d in range(qos_dims):
            for u in range(m):
                q = 0
                for c in range(n):
                    chosen = 0
                    load = np.zeros(len(tp[c]))
                    for i, s in enumerate(services[c]):
                        if x[u][c][i].value():
                            load[s] += x[u][c][i].value()
                            q += x[u][c][i].value() * qos[c][d][u, s]
                            chosen += x[u][c][i].value()
                    if abs(chosen - user_class_demand[u][c]) > 1e-5 or\
                            np.any(load > tp[c]):
                        wrong = True
                satisfied += (q <= user_reqs[u][d])
                if user_reqs[u][d]:
                    deviations[d].append((user_reqs[u][d] - q) / abs(user_reqs[u][d]))
        if exhausted or (not wrong and satisfied == m * qos_dims):
            print(LpStatus[prob.status], file=sys.stderr)
            if LpStatus[prob.status] == 'Undefined':
                return None
            return satisfied / (m * qos_dims), [np.mean(x) for x in deviations]
        r += 1
