from copy import deepcopy
from transport3 import transport
from random import randrange
from bisect import bisect_left, bisect_right
from multiprocessing import cpu_count, Pool
import numpy as np
from util import *
from time import time
from math import exp, floor, ceil
from concurrent.futures import ProcessPoolExecutor
import sys

def percentile(a, i):
    if len(a) == 1:
        return a[0]
    x = i * (len(a) - 1) / 100
    lo, hi = floor(x), ceil(x)
    k, l = x - lo, hi - x
    if k < 1e-3:
        return a[lo]
    if l < 1e-3:
        return a[hi]
    perc = k * a[lo] + l * a[hi]
    return perc

INFEASIBLE = 10**9

@timeit
def solve(test, tsm):
    if test[1] == 1:
        return solve_single_task(test, tsm)
    return solve_composite(test, tsm)

def solve_single_task(test, tsm):
    m, n, qos_dims, user_reqs, _, qos, tp = test
    qos, supply = qos[0], tp[0]
    n = len(supply)
    cost = np.zeros((n, m))
    for i in range(n):
        for u in range(m):
            cost[i, u] = sum(user_reqs[u, d] < qos[d][u, i] for d in range(qos_dims))
    demand = [1 for i in range(m)]
    x = transport((supply, demand, cost, tsm))
    if x.shape[1] == m + 1:
        satisfied = np.sum(x[:, :-1] * (qos_dims - cost)) / (m * qos_dims)
    else:
        satisfied = np.sum(x[:, :] * (qos_dims - cost)) / (m * qos_dims)
    return satisfied

def solve_composite(test, tsm):
    m, n, qos_dims, user_reqs, user_class_demand, qos, tp = test
    services_in_class = [len(tp[c]) for c in range(n)]

    md = [[-np.ones((qos_dims, services_in_class[c]))
            for c in range(n)] for u in range(m)]
    w = np.ones((m, qos_dims))
    u_index = [[
            bisect_left([u1 for u1 in range(m) if user_class_demand[u1][c]], u)
            for c in range(n)] for u in range(m)]

    # Compute MD - matching difficulty MD_d(u, c, i) for each user.
    percentiles = list(range(0, 101, 10))
    avg_md, md_count = 0, 0
    for d in range(qos_dims):
        for u in range(m):
            # bad --> good
            sorted_qos = [np.sort(qos[c][d][u, :])[::-1] for c in range(n)]
            # cheap --> expensive (reverse)
            sorted_services = [np.argsort(qos[c][d][u, :]) for c in range(n)]

            q_for_perc = []
            for perc in percentiles:
                q = 0
                for c in range(n):
                    if user_class_demand[u][c]:
                        val = percentile(sorted_qos[c], perc)
                        q += user_class_demand[u][c] * val
                q_for_perc.append(q)

            for c in range(n):
                if user_class_demand[u][c] == 0:
                    continue
                perc_gain = [q_for_perc[perc_index]
                        - user_class_demand[u][c] * percentile(sorted_qos[c], perc)
                        for perc_index, perc in enumerate(percentiles)]
                perc_index = 0
                for i in sorted_services[c]:
                    if perc_index == len(percentiles):
                        break
                    while perc_index < len(percentiles):
                        q = perc_gain[perc_index] + user_class_demand[u][c] * qos[c][d][u, i]
                        if user_reqs[u, d] >= q:
                            md[u][c][d, i] = percentiles[perc_index] / 100
                            avg_md += md[u][c][d, i]
                            md_count += 1
                            break
                        perc_index += 1

    avg_md /= md_count
    for u in range(m):
        for c in range(n):
            md[u][c][np.where(md[u][c] == -1)] = INFEASIBLE

    max_satisfied = 0
    satisfied_it = np.ones(9)
    dev_it = [-1 for it in range(9)]

    iteration = 0
    while True:
        # Create transportation problems.
        pars = []
        for c in range(n):
            cost = np.stack([np.matmul(w[u], md[u][c]) #/ np.sum(w[u])\
                    for u in range(m) if user_class_demand[u][c]]).T
            demand = [user_class_demand[u][c] for u in range(m) if user_class_demand[u][c]]
            supply = deepcopy(tp[c])
            pars.append((supply, demand, cost, tsm))

        # Solve transportation problems.
        with ProcessPoolExecutor(4) as p:
            matching = list(p.map(transport, pars))

        # Evaluate the global solution and update weights.
        w_increment = 1 + exp(3 - iteration)
        satisfied = 0
        deviations = [[] for d in range(qos_dims)]
        for u in range(m):
            for d in range(qos_dims):
                q = 0
                for c, result in enumerate(matching):
                    if user_class_demand[u][c] == 0:
                        continue
                    chosen = 0
                    for i in range(services_in_class[c]):
                        q += result[i][u_index[u][c]] * qos[c][d][u, i]
                        chosen += result[i][u_index[u][c]]
                    assert chosen == user_class_demand[u][c]
                if user_reqs[u, d] < q:
                    w[u, d] *= w_increment
                else:
                    satisfied += 1
                if user_reqs[u][d]:
                    deviations[d].append((user_reqs[u][d] - q) / abs(user_reqs[u][d]))

        max_satisfied = max(max_satisfied, satisfied)
        satisfied_it[iteration] = max_satisfied / (m * qos_dims)
        for it in range(iteration, 9):
                dev_it[it] = np.mean([np.mean(x) for x in deviations])
        if w_increment < 1.01 or satisfied == m * qos_dims:
            return max_satisfied / (m * qos_dims),\
                    [np.mean(x) for x in deviations], avg_md,\
                    satisfied_it, dev_it
        iteration += 1
