from copy import deepcopy
from pulp import *
from numpy.linalg import norm
from sklearn.cluster import KMeans
import numpy as np
import sys
from util import *
from time import time

@timeit
def solve(test):
    m, n, qos_dims, user_reqs, user_class_demand, qos, tp = test
    users_per_class = np.zeros(n, dtype=int)

    ranking = [None for c in range(n)]
    for c in range(n):
        services = list(range(len(tp[c])))
        utility = np.zeros(len(services))
        for u in range(m):
            if user_class_demand[u][c]:
                users_per_class[c] += 1
                for d in range(qos_dims):
                    best_for_u = sorted(services, key=lambda i: qos[c][d][u, i])
                    for pos, i in enumerate(best_for_u):
                        utility[i] += pos
        ranking[c] = sorted(services, key=lambda i: utility[i])

    r = 1
    while True:
        exhausted = True
        prob = LpProblem('clus_mip', LpMinimize)
        services = [[] for c in range(n)]
        num_s = [0 for c in range(n)]
        for c in range(n):
            k = r * 10  #users_per_class[c]
            if k < len(ranking[c]):
                exhausted = False
            else:
                k = len(ranking[c])
            services[c] = ranking[c][:k]
            num_s[c] = len(services[c])
        x = LpVariable.dicts('choice',
                (list(range(m)), list(range(n)), list(range(max(num_s)))),
                0, 1, LpInteger)
        for u in range(m):
            for c in range(n):
                prob += lpSum(x[u][c][i] for i in range(num_s[c])) == user_class_demand[u][c], ""
        for c in range(n):
            for i, s in enumerate(services[c]):
                prob += lpSum(x[u][c][i] for u in range(m)) <= tp[c][s], ""
        for u in range(m):
            for d in range(qos_dims):
                prob += (float(user_reqs[u][d]) >= lpSum(
                    lpSum(qos[c][d][u, s] * x[u][c][i] for i, s in enumerate(services[c]))
                    for c in range(n)
                    ))
        prob.solve()

        # Check the solution.
        satisfied = 0
        wrong = False
        deviations = [[] for d in range(qos_dims)]
        for d in range(qos_dims):
            for u in range(m):
                q = 0
                for c in range(n):
                    chosen = 0
                    load = np.zeros(len(tp[c]))
                    for i, s in enumerate(services[c]):
                        if x[u][c][i].value():
                            load[s] += x[u][c][i].value()
                            q += x[u][c][i].value() * qos[c][d][u, s]
                            chosen += x[u][c][i].value()
                    if abs(chosen - user_class_demand[u][c]) > 1e-5 or\
                            np.any(load > tp[c]):
                        wrong = True
                satisfied += (q <= user_reqs[u][d])
                if user_reqs[u][d]:
                    deviations[d].append((user_reqs[u][d] - q) / abs(user_reqs[u][d]))
        if exhausted or (not wrong and satisfied == m * qos_dims):
            print(r, LpStatus[prob.status], file=sys.stderr)
            if LpStatus[prob.status] == 'Undefined':
                return None
            return satisfied / (m * qos_dims), [np.mean(x) for x in deviations]
        r += 1
