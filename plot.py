import sys
import os
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from copy import deepcopy
from matplotlib.ticker import MaxNLocator


BROJEVI = False
 
tip = int(sys.argv[1])
filename = ('single_task', 'eq_qos', 'general', 'general', 'general')[tip] + '.out'
if len(sys.argv) > 2:
    filename = sys.argv[2]
tezine = (2, 3, 3, 3, 3)[tip]
with open(filename, 'r') as f:
    lines = f.readlines()
i = 0
res = [[[] for tez in range(5)] for alg in range(5)]
time = [[[] for tez in range(5)] for alg in range(5)]
dev = [[[] for tez in range(5)] for alg in range(5)]
md = [[[] for tez in range(5)] for alg in range(5)]
sat_it = [[[] for tez in range(5)] for alg in range(5)]
dev_it = [[[] for tez in range(5)] for alg in range(5)]
while '---\n' in lines:
    j = lines.index('---\n')
    lines[j] = None
    if j - i < (10, 18, 12, 12, 12)[tip]:
        i = j + 1
        continue
    for tezina in range(tezine):
        alg = 0
        while lines[i].strip() != '':
            dijelovi = lines[i].split('--')
            if tip >= 3 and alg > 0:
                try:
                    r, t, d, matching_difficulty = map(float, dijelovi[0].split())
                    res[alg][tezina].append(r)
                    time[alg][tezina].append(t)
                    dev[alg][tezina].append(d)
                    md[alg - 1][tezina].append(matching_difficulty)
                    sat_it[alg - 1][tezina].append(np.array(list(map(float, dijelovi[1].split()))))
                    dev_it[alg - 1][tezina].append(np.array(list(map(float, dijelovi[2].split()))))
                except:
                    pass
            else:
                r, t, d = map(float, dijelovi[0].split()[:3])
                res[alg][tezina].append(r)
                time[alg][tezina].append(t)
                dev[alg][tezina].append(d)
            alg += 1
            i += 1
        i += 1
    i = j + 1

if tip == 0 or tip == 3:
    font = {'size': 15}
else:
    font = {'size': 15}
matplotlib.rc('font', **font)

def label_bar(ax, bars, text_format, is_inside=True, **kwargs):
    if not BROJEVI:
        return
    max_y_value = max(bar.get_height() for bar in bars)
    if is_inside:
        distance = max_y_value * 0.05
    else:
        distance = max_y_value * 0.001
    for bar in bars:
        #print(bar.get_height())
        text = text_format.format(bar.get_height())
        text_x = bar.get_x() + bar.get_width() / 2
        if is_inside:
            text_y = bar.get_height() - distance
        else:
            text_y = bar.get_height() + distance
        ax.text(text_x, text_y, text, ha='center', va='bottom', **kwargs)

def plotaj(a, measure, ymin=0, ymax=None):
    if tip == 0:
        algs = ['MIP', 'AP', 'SS-VAM', 'SS-TSM']
        ticks = ['Low THR', 'High THR']
        cols = ['red', 'pink', 'green', 'blue']
    elif tip == 1:
        algs = ['MIP', 'Clus2-MIP', 'Clus3-MIP', 'SS-VAM', 'SS-TSM']
        cols = ['red', 'purple', 'orange', 'g', 'b']
        ticks = ['Easy QoS reqs.', 'Medium QoS reqs.', 'Hard QoS reqs.']
    elif tip == 2:
        algs = ['MIP', 'SS-VAM', 'SS-TSM', 'GreedyMIP']
        cols = ['r', 'g', 'b', 'm']
        ticks = ['Easy QoS reqs.', 'Medium QoS reqs.', 'Hard QoS reqs.']
    elif tip == 3:
        algs = ['SS-VAM', 'SS-TSM']
        cols = ['g', 'b']
        ticks = ['Easy QoS reqs.', 'Medium QoS reqs.', 'Hard QoS reqs.']
    n = len(algs)
    m = len(ticks)

    mid = np.zeros(m)
    y = []
    for alg in range(n):
        x, positions = [], []
        for tez in range(m):
            x.append(sum(a[alg][tez]) / len(a[alg][tez]))
            positions.append(tez * (n + 1) + alg)
            mid[tez] += positions[-1]
        bars = plt.bar(positions, x, align='center', color=cols[alg], label=algs[alg])
        label_bar(plt, bars, "{:0.3f}", is_inside=False, size=10)
        y += x
    if len(algs) > 2 and tip == 1:
        plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3, fontsize=9.5,
                       ncol=n, mode="expand", borderaxespad=0.)
    elif len(algs) > 2 and tip not in [0, 3]:
        plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3, fontsize=12,
                       ncol=n, mode="expand", borderaxespad=0.)
    else:
        plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3, fontsize=13,
                       ncol=n, mode="expand", borderaxespad=0.)
    if len(ticks) > 2 and tip:
        plt.xticks(mid / n, ticks, fontsize=12)
    else:
        plt.xticks(mid / n, ticks)
    if not ymax:
        ymax = max(y) * 1.01
    if measure == 'accuracy':
        plt.ylabel('Avg. percentage of satisfied QoS reqs.')
    elif measure == 'time':
        plt.ylabel('Avg. execution time [s]')
    elif measure == 'utility':
        plt.ylabel('Avg. matching difficulty')
    elif measure == 'deviation':
        plt.ylabel('Avg. QoS improvement')
    plt.axis([-1, m * (n + 1) - 1, ymin, ymax])
    #plt.show()
    plt.gcf().subplots_adjust(left=0.15)
    plt.savefig(('Single_Task_Experiment', 'Indep_QoS_Experiment', 'General_Experiment',
        'General_Experiment')[tip] +
            '_' + measure + '.pdf', format='pdf')
    plt.close()

def plotaj_it(a, ylabel, measure):
    ticks = ['Easy QoS reqs.', 'Medium QoS reqs.', 'Hard QoS reqs.']
    plt.rcParams["figure.figsize"] = [14, 7]
    plt.rcParams["font.size"] = 20
    for tez in range(3):
        plt.subplot(130 + 1 + tez)
        p = plt.plot(np.arange(0., 9., 1.), sum(a[0][tez]) / len(a[0][tez]),
                'g-o', fillstyle='none', label='SS-VAM')
        x, y = p[0].get_data()
        for i, j in zip(x, y):
            if BROJEVI:
                plt.annotate("{:.3f}".format(j), xy=(i, j), size=12, )
        p = plt.plot(np.arange(0., 9., 1.), sum(a[1][tez]) / len(a[1][tez]),
                'b-o', label='SS-TSM')
        x, y = p[0].get_data()
        for i, j in zip(x, y):
            if BROJEVI:
                plt.annotate("{:.3f}".format(j), xy=(i, j), size=12, )
        plt.title(ticks[tez])
        if tez == 0:
            plt.ylabel(ylabel, fontsize=20)
        plt.xlabel('Iterations', fontsize=20)
        if measure == 'accuracy':
            plt.axis(([-0.1, 8.2, 0.98, 1.0005], [-0.1, 8.2, 0.92, 1.002],
                [-0.1, 8.2, 0.6, 1.01])[tez])
        else:
            plt.axis(([-0.1, 8.2, 0.1, 1.0], [-0.1, 8.2, 0.1, 1.0],
                [-0.1, 8.2, 0.1, 1.0])[tez])
        plt.xticks([0,1,2,3,4,5,6,7,8], [0,1,2,3,4,5,6,7,8])
        if tez == 0: 
            plt.legend(loc=6, fontsize=20)
        if measure == 'accuracy':
            if tez == 0:
                plt.yticks([0.980, 0.985, 0.990, 0.995, 1.000])
            if tez == 1:
                plt.yticks([0.92, 0.93, 0.94, 0.95, 0.96, 0.97, 0.98, 0.99, 1.00])
            if tez == 2:
                plt.yticks([0.6, 0.7, 0.8, 0.9, 1.0])
    #plt.show()
    plt.gcf().subplots_adjust(left=0.1)
    plt.savefig('General_Experiment_{}_iterations.pdf'.format(measure), format='pdf')
    plt.close()

if tip < 3:
    plotaj(res, 'accuracy', (0.95, 0.95, 0.95)[tip], 1.002)
    plotaj(time, 'time',)
    if tip: plotaj(dev, 'deviation', 0, 1)
elif tip == 3:
    plotaj(md, 'utility', 0, 1)
elif tip == 4:
    plotaj_it(sat_it, 'Percentage of satisfied QoS reqs.', 'accuracy')
    plotaj_it(dev_it, 'Average QoS improvement', 'deviation')
